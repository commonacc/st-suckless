# Fork of [suckless terminal (st)](https://st.suckless.org/) with the following patches:

+ Xresources
+ Alpha
+ Copy to clipboard (ctrl-shift-c)
+ Shift+Mouse wheel scrollback
+ Vertcenter
+ Dark solarized colors
+ Version 0.8.1

## Installation
```
make clean
make
sudo make install
```

